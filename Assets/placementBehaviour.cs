﻿using UnityEngine;
using System.Collections;

public class placementBehaviour : MonoBehaviour
{

	private CodeController controller;

	// Use this for initialization
	void Start ()
	{
		controller = GameObject.Find("Character").GetComponent<CodeController>();

	}

	void OnMouseDown()
	{
		Debug.Log("placing object");
		controller.PlaceObject();
	}
}

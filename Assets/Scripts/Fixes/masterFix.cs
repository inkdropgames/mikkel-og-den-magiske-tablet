﻿using UnityEngine;
using System.Collections;

public class masterFix : MonoBehaviour
{

	public GameObject master;

	// Use this for initialization
	void Start () {
		if (GameObject.Find("_Master") == null)
		{
			master.SetActive(true);
		}
	}

	public void loadLevel()
	{
		master = GameObject.Find("_Master");
		master.GetComponent<MasterController>().LoadLevel();
	}

	public void login()
	{
		master = GameObject.Find("_Master");
		master.GetComponent<MasterController>().LoginButton();
	}


}

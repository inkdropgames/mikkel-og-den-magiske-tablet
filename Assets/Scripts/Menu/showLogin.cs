﻿using UnityEngine;
using System.Collections;

public class showLogin : MonoBehaviour
{
	public MasterController master;
	public GameObject loginUI;

	void Start ()
	{
		StartCoroutine(loginFix());
	}

	IEnumerator loginFix()
	{
		yield return new WaitForSeconds(.01f);
		master = GameObject.Find("_Master").GetComponent<MasterController>();

		if (master.username == "")
		{
			loginUI.SetActive(true);
		}
		else
		{
			loginUI.SetActive(false);
		}
	}
	
}

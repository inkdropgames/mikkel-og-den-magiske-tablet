﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
	public AudioClip menuSound;

	void OnMouseDown()
	{
		switch (tag)
		{
			case "Spil":
				StartCoroutine(LoadLevel(1));
				break;
			case "Afslut":
				StartCoroutine(QuitGame());
				break;
			case "Info":
				StartCoroutine(LoadLevel(8));
				break;
		}

		transform.parent.GetComponent<Animation>().Play("MenuOut");
		GetComponent<AudioSource>().PlayOneShot(menuSound);
	}

	void OnMouseEnter() { GetComponent<SpriteRenderer>().color = Color.red; }

	void OnMouseExit() { GetComponent<SpriteRenderer>().color = Color.white; }

    public void GoToMenu() { Application.LoadLevel("mainmenu"); }

    public void GoToSelector() { Application.LoadLevel("level_selection"); }

    IEnumerator LoadLevel(int level)
    {
        yield return new WaitForSeconds(1.3f);
        Application.LoadLevel(level);
		Debug.Log("loaded level: " + level);
	}

    IEnumerator QuitGame()
    {
        yield return new WaitForSeconds(1.3f);
        Application.Quit();
    }
}

﻿using System.Net.Mime;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class levelSelectionBehaviour : MonoBehaviour
{

	public MasterController master;
	public Text goal;
	public Text myHighScore;

	public GameObject[] Stars;

	public GameObject levelboard;

	public GameObject star1;
	public GameObject star2;
	public GameObject star3;

	public Text[] highscore;

	public Text[] highscoreName;

	public AudioClip menuSound;


	private void start()
	{

	}

	// Use this for initialization
	private void OnMouseDown()
	{
		master = GameObject.Find("_Master").GetComponent<MasterController>();
		if (tag == "Level1")
		{
			goal.text = "Mål: " + master.levelGoals[0].ToString();
			myHighScore.text = master.levelScores[0].ToString();
			checkStars(0, 8000, 8500, 9000);
			setHighscores(1);
			if (master.completedLevels >= 0)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 1;
				
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}

		if (tag == "Level2")
		{
			goal.text = "Mål: " + master.levelGoals[1].ToString();
			myHighScore.text = master.levelScores[1].ToString();
			checkStars(1, 8000, 8500, 9000);
			setHighscores(2);
			if (master.completedLevels >= 1)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 2;
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}
		if (tag == "Level3")
		{
			goal.text = "Mål: " + master.levelGoals[2].ToString();
			myHighScore.text = master.levelScores[2].ToString();
			checkStars(2, 8000, 8500, 9000);
			setHighscores(3);
			if (master.completedLevels >= 2)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 3;
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}
		if (tag == "Level4")
		{
			goal.text = "Mål: " + master.levelGoals[3].ToString();
			myHighScore.text = master.levelScores[3].ToString();
			checkStars(3, 8000, 8500, 9000);
			setHighscores(4);
			if (master.completedLevels >= 3)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 4;
			}
		}
		if (tag == "Level5")
		{
			goal.text = "Mål: " + master.levelGoals[4].ToString();
			myHighScore.text = master.levelScores[4].ToString();
			checkStars(4, 8000, 8500, 9000);
			setHighscores(5);
			if (master.completedLevels >= 4)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 5;
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}
		if (tag == "Level6")
		{
			goal.text = "Mål: " + master.levelGoals[5].ToString();
			myHighScore.text = master.levelScores[5].ToString();
			checkStars(5, 8000, 8500, 9000);
			setHighscores(6);
			if (master.completedLevels >= 5)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 6;
			}
		}
		if (tag == "Level7")
		{
			goal.text = "Mål: " + master.levelGoals[6].ToString();
			myHighScore.text = master.levelScores[6].ToString();
			checkStars(6, 8000, 8500, 9000);
			setHighscores(7);
			if (master.completedLevels >= 6)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 7;
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}
		if (tag == "Level8")
		{
			goal.text = "Mål: " + master.levelGoals[7].ToString();
			myHighScore.text = master.levelScores[7].ToString();
			checkStars(7, 8000, 8500, 9000);
			setHighscores(8);
			if (master.completedLevels >= 7)
			{
				levelboard.SetActive(true);
				master.selectedLevel = 8;
			}
			GetComponent<AudioSource>().PlayOneShot(menuSound);
		}
		//levelboard.SetActive(true);

	}

	private void OnMouseEnter()
	{
		//gameObject.transform.localScale = transform.localScale*1.2f;
		gameObject.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
		
	}

	private void OnMouseExit()
	{
		gameObject.transform.localScale = transform.localScale*0.8f;
		gameObject.transform.localScale = new Vector3(0.08f, 0.08f, 0.08f);
	}

	private void checkStars(int levelNumber, int starpoint1, int starpoint2, int starpoint3)
	{
		//int numberOfStars = master.levelScores[levelNumber];
		if (master.levelScores[levelNumber] < starpoint2)
		{
			star1.GetComponent<Image>().color = Color.white;
			star2.GetComponent<Image>().color = Color.black;
			star3.GetComponent<Image>().color = Color.black;	
		}
		else if (master.levelScores[levelNumber] < starpoint3 && master.levelScores[levelNumber] > starpoint2)
		{
			star1.GetComponent<Image>().color = Color.white;
			star2.GetComponent<Image>().color = Color.white;
			star3.GetComponent<Image>().color = Color.black;
		}
		else if(master.levelScores[levelNumber] > starpoint3)
		{
			star1.GetComponent<Image>().color = Color.white;
			star2.GetComponent<Image>().color = Color.white;
			star3.GetComponent<Image>().color = Color.white;	
		}

		if (master.levelScores[levelNumber] == 0)
		{
			star1.GetComponent<Image>().color = Color.black;
			star2.GetComponent<Image>().color = Color.black;
			star3.GetComponent<Image>().color = Color.black;	
		}

	}

	void setHighscores(int level)
	{
		for (int i = 3; i > 0; i--)
		{
			highscore[i - 1].text = PlayerPrefs.GetInt(level + "highscore" + i).ToString();
			highscoreName[i - 1].text = PlayerPrefs.GetString(level + "highscoreName" + i);
		}
		//highscore[0].text = PlayerPrefs.GetInt(level + "highscore" + 1).ToString();
		//highscore[1].text = PlayerPrefs.GetInt(level + "highscore" + 2).ToString();
		//highscore[2].text = PlayerPrefs.GetInt(level + "highscore" + 3).ToString();
		//highscoreName[0].text = PlayerPrefs.GetString(level + "highscoreName" + 1);
		//highscoreName[1].text = PlayerPrefs.GetString(level + "highscoreName" + 2);
		//highscoreName[2].text = PlayerPrefs.GetString(level + "highscoreName" + 3);

	}
}

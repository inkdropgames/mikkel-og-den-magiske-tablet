﻿using UnityEngine;
using System.Collections;

public class DeathBehaviour : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.name == "Character")
			Application.LoadLevel(Application.loadedLevel);

	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "Character")
			Application.LoadLevel(Application.loadedLevel);

	}
}

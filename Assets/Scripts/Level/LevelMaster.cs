﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelMaster : MonoBehaviour
{

	private MasterController master;
	public int levelNumber;
	public int score;
	public int startPoints;
	public float playerScore;

	public InputField scoreInput;

	public GameObject scoreScreen;
	public Text scoreObject;

	public int stars3;
	public int stars2;
	public int stars1;

	public GameObject star1;
	public GameObject star2;
	public GameObject star3;

	public GameObject depleteScoreText;

	// Use this for initialization
	void start()
	{
		master = GameObject.Find("_Master").GetComponent<MasterController>() as MasterController;
		playerScore = (float)startPoints;
	}

	void Update()
	{
		playerScore -= Time.deltaTime*10;
	}

	public void completeLevel(int score)
	{
		
		master = GameObject.Find("_Master").GetComponent<MasterController>();
		master.levelCompleted(levelNumber, score);
		scoreScreen.SetActive(true);
		scoreObject.text = score.ToString();
		master.CheckHighscore(score, levelNumber);
		if (score < stars3)
		{
			if (score < stars2)
			{
				star1.GetComponent<SpriteRenderer>().color = Color.white;
				if (PlayerPrefs.GetInt(master.username + levelNumber + "stars") == 1)
				{
					PlayerPrefs.SetInt(master.username + levelNumber + "stars", 1);	
				}
				
			}
			else
			{
				star1.GetComponent<SpriteRenderer>().color = Color.white;
				star2.GetComponent<SpriteRenderer>().color = Color.white;
				if (PlayerPrefs.GetInt(master.username + levelNumber + "stars") < 2)
				{
					PlayerPrefs.SetInt(master.username + levelNumber + "stars", 2);
				}
			}
		}
		else
		{
			star1.GetComponent<SpriteRenderer>().color = Color.white;
			star2.GetComponent<SpriteRenderer>().color = Color.white;
			star3.GetComponent<SpriteRenderer>().color = Color.white;
			if (PlayerPrefs.GetInt(levelNumber + "stars") < 3)
			{
				PlayerPrefs.SetInt(levelNumber + "stars", 3);
			}
		}
		//Application.LoadLevel(1);
		PlayerPrefs.Save();
	}

	public void depleteScore(int amount)
	{
		playerScore -= amount;
		if (amount == 400)
		{
			GameObject clone = Instantiate(Resources.Load("200pts"), depleteScoreText.transform.position, Quaternion.identity) as GameObject;
			clone.transform.parent = depleteScoreText.transform;
			Destroy(clone.gameObject, 2);
		}
		else
		{
			GameObject clone = Instantiate(Resources.Load("500pts"), depleteScoreText.transform.position, Quaternion.identity) as GameObject;
			clone.transform.parent = depleteScoreText.transform;
			Destroy(clone.gameObject, 2);
		}
		
	}

	public void replayLevel()
	{
		Application.LoadLevel(Application.loadedLevel);
	}

	public void menu()
	{
		Application.LoadLevel(1);
	}

}

﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour
{

	public LevelMaster levelmaster;

	// Use this for initialization
	void Start ()
	{
		levelmaster = GameObject.Find("LevelManager").GetComponent<LevelMaster>();
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.GetComponent<Collider2D>().name == "Character")
		{
			int playerScore = (int) levelmaster.playerScore;
			levelmaster.completeLevel(playerScore);
			Time.timeScale = 0;
		}
	}
}

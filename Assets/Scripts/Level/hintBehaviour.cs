﻿using UnityEngine;
using System.Collections;

public class hintBehaviour : MonoBehaviour
{

    public GameObject hint;
    public GameObject hintButton;

    void Start()
    {
       StartCoroutine(Countdown());
    }

    public void Hint()
    {
        StartCoroutine(showHint());
    }
    
    IEnumerator showHint()
    {
        hint.SetActive(true);
        yield return new WaitForSeconds(10);
        hint.SetActive(false);

    }

    IEnumerator Countdown()
    {
        yield return new WaitForSeconds(3);
        hintButton.SetActive(true);
        yield return new WaitForSeconds(10);
        hintButton.SetActive(false);
    }
	
}

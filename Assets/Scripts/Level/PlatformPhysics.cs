﻿using UnityEngine;
using System.Collections;

public class PlatformPhysics : MonoBehaviour {

	// Use this for initialization
	private Transform player;
	private bool overPlatform;

	void Start ()
	{
		player = GameObject.Find("Character").transform;
	}
	
	// Update is called once per frame
	void Update () {

		if (player.position.y - 0.5f < transform.position.y)
		{
			overPlatform = true;
		}
		else
		{
			overPlatform = false;
		}
		Physics2D.IgnoreLayerCollision(10, 9, overPlatform);	
	}
}

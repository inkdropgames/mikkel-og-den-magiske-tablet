﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovementController : MonoBehaviour {

    private Animator _anim;
    private bool _onLadder;
	private int _direction;

	void Start()
    {
        _anim = GetComponent<Animator>();
    }

    void Update()
    {
	    if (!GetComponent<CodeController>().codeArea.activeInHierarchy)
	    {
		    if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) _direction = -1;
		    else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) _direction = 1;
		    else _direction = 0;

		    _anim.SetInteger("Direction", _direction);
		    transform.Translate(new Vector2(_direction, 0)*Time.deltaTime*3);

		    if (Input.GetKey(KeyCode.UpArrow) && _onLadder || Input.GetKey(KeyCode.W) && _onLadder)
		    {
			    transform.Translate(Vector2.up*Time.deltaTime*3);
			    GetComponent<Rigidbody2D>().gravityScale = 0;
		    }
		    else GetComponent<Rigidbody2D>().gravityScale = 1;

		    if (Input.GetMouseButton(0) && !GetComponent<CodeController>()._placeObject)
		    {
			    var targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			    targetPos.z = transform.position.z;
			    if (!_onLadder)
			    {
				    targetPos.y = transform.position.y;
					GetComponent<Rigidbody2D>().gravityScale = 1;
				}
			    else
			    {
				    GetComponent<Rigidbody2D>().gravityScale = 0;
			    }
			    transform.position = Vector3.MoveTowards(transform.position, targetPos, 3.5f*Time.deltaTime);

				if(Camera.main.ScreenToWorldPoint(Input.mousePosition).x > transform.position.x)
					_anim.SetInteger("Direction", 1);
				else if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x)
					_anim.SetInteger("Direction", -1);
			}
		    else
		    {
			    _direction = 0;
		    }
	    }
    }

    void OnTriggerEnter2D(Collider2D other) { _onLadder = other.tag == "Ladder"; }
	void OnTriggerExit2D(Collider2D other)	{ _onLadder = other.tag != "Ladder"; }
}

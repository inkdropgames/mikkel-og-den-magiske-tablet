﻿using System.Net.Mime;
using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;

public class MasterController : MonoBehaviour
{
	[Header("User")]
	public string username;
	public InputField usernameInput;
	public bool loggedIn = false;

	[Header("Level")]
	public GameObject[] levels;
	public int[] levelScores = new int[] { 0000, 0000, 0000, 0000, 0000, 0000, 0000, 0000 };
	public int[] levelGoals = new int[] { 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000 };
	public int completedLevels;
	public int selectedLevel;

	
	[Header("UI")]
	public GameObject loginUI;
	public GameObject loginError;
	
	//Makes sure the GameObject won't destroy itself when changing scenes
	void Awake() { DontDestroyOnLoad(transform.gameObject); }

	//Saves the scores when application is closed
	void OnApplicationQuit() { PlayerPrefs.Save(); }

	//Load the selected level
	public void LoadLevel() { Application.LoadLevel(selectedLevel + 1); }

	//Login the user and retrieve relevant data
	public void Login()
	{

		if (!loggedIn)
			loggedIn = true;

		//Get the logged in player's scores from the save file
		for (int i = 0; i < levelScores.Length; i++)
			levelScores[i] = PlayerPrefs.GetInt(username + "levelScore" + i);

		completedLevels = ScoreCheck();

		//Grey out all levels so we only highlight the unlocked ones
		foreach (GameObject sprite in levels)
			sprite.GetComponent<SpriteRenderer>().color = Color.gray;

		//Show unlocked levels
		for (int i = 0; i <= completedLevels; i++)
			levels[i].GetComponent<SpriteRenderer>().color = Color.white;

		GameObject.Find("Username").GetComponent<Text>().text = username;
	}

	//Runs everytime we load a new scene
	void OnLevelWasLoaded(int level)
	{
		Time.timeScale = 1;

		//We only need this for the level selector so we return 'null' for all other scenes
		if (level != 1)
			return;

		//Find all the level markers and place them in an array
		for (int i = 1; i <= levels.Length; i++)
			levels[i - 1] = GameObject.Find("Level_0" + i);

		Login();
		
	}

	//Runs when player completes a level and adds the score to the save file
	public void levelCompleted(int level, int score)
	{
		levelScores[level] = score;
		PlayerPrefs.SetInt(username + "completedLevels", level);

		//Checks if player already has a higher score
		if (PlayerPrefs.GetInt(username + "levelScore" + (level - 1)) < score)
			PlayerPrefs.SetInt(username + "levelScore" + (level - 1), score);
	}
	
	//Gives the player the ability to change user without relaunching the game
	public void LoginButton()
	{
	    loginUI = GameObject.Find("Login");
		for (int i = 0; i < loginUI.transform.childCount; i++)
		{
			if (loginUI.transform.GetChild(i).name == "taleboble")
				loginError = loginUI.transform.GetChild(i).gameObject;
		}

		//Makes sure the username field is not empty
		if (usernameInput.text != "")
		{
			loginError.SetActive(false);
			username = usernameInput.text;
			loginUI.SetActive(false);
			Login(); 
		}
		else loginError.SetActive(true);

	}

	//Returns the top 3 highscores for a given level
	public void CheckHighscore(int score, int level)
	{
		if(PlayerPrefs.GetInt(level + "highscore" + 1) < score)
		{
			PlayerPrefs.SetInt(level + "highscore" + 3, PlayerPrefs.GetInt(level + "highscore" + 2));
			PlayerPrefs.SetString(level + "highscoreName" + 3, PlayerPrefs.GetString(level + "highscoreName" + 2));
			PlayerPrefs.SetInt(level + "highscore" + 2, PlayerPrefs.GetInt(level + "highscore" + 1));
			PlayerPrefs.SetString(level + "highscoreName" + 2, PlayerPrefs.GetString(level + "highscoreName" + 1));
			PlayerPrefs.SetInt(level + "highscore" + 1, score);
			PlayerPrefs.SetString(level + "highscoreName" + 1, username);
		}
		else if (PlayerPrefs.GetInt(level + "highscore" + 1) > score && score > PlayerPrefs.GetInt(level + "highscore" + 2))
		{
			PlayerPrefs.SetInt(level + "highscore" + 3, PlayerPrefs.GetInt(level + "highscore" + 2));
			PlayerPrefs.SetString(level + "highscoreName" + 3, PlayerPrefs.GetString(level + "highscoreName" + 2));
			PlayerPrefs.SetInt(level + "highscore" + 2, score);
			PlayerPrefs.SetString(level + "highscoreName" + 2, username);
		}
		else if (PlayerPrefs.GetInt(level + "highscore" + 3) < score && score < PlayerPrefs.GetInt(level + "highscore" + 2))
		{
			PlayerPrefs.SetInt(level + "highscore" + 3, score);
			PlayerPrefs.SetString(level + "highscoreName" + 3, username);	
		}
	}

	//Return number of completed levels
    int ScoreCheck()
    {
	    int completed = levelScores.Count(score => score > 0);

		//If calculated data doesn't match save file, it could be corrupted.
	    if (PlayerPrefs.GetInt(username + "completedLevels") != completedLevels)
            Debug.Log("Corrupted Profile");

	    return completed;
    }


}

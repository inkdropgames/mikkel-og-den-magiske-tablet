﻿using UnityEngine;
using System.Collections;

public class HoverBehaviour : MonoBehaviour
{
	public bool canHover;
	private GameObject glowObject;
	private CodeController master;

	public bool doDestroy;
	public bool doTranslate;
	public bool doRotate;
	public bool changeGravity;
    public bool stateEnable;
    public bool stateDisable;

	//private float cooldownFix = 4.0f;
	public float fixTimer = 0;


	void Start()
	{
		master = GameObject.Find("Character").GetComponent<CodeController>();
	}

	void FixedUpdate()
	{
		fixTimer += Time.deltaTime;
	}

	// Use this for initialization
	void OnMouseEnter ()
	{
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
			return;
		if(canHover)
		{
			for(int i=0;i<transform.childCount;i++)
			{
				if (transform.GetChild(i).name == "GLOW")
				{
					glowObject = transform.GetChild(i).gameObject;	
				}
			}
			glowObject.SetActive(true);
		}
	}

	void OnMouseExit()
	{
		glowObject.SetActive(false);
	}

	void OnMouseDown()
	{
		if(canHover && !master.usedCommand)
		{
			if(doDestroy)
			{
				master.DestroyObject(gameObject);
				usedCommand();
				//doDestroy = false;
			}
			if (doTranslate)
			{
				master.TranslateObject(gameObject);
				StartCoroutine(fix());
				fixTimer = 0;
				master.ResetColors();
				usedCommand();
				//doTranslate = false;
			}
			if (doRotate)
			{
				master.RotateObject(gameObject);
				StartCoroutine(fix());
				fixTimer = 0;
				master.ResetColors();
				usedCommand();
				//doRotate = false;	
			}
			if (changeGravity)
			{
				master.ChangeGravity(gameObject);
				StartCoroutine(fix());
				fixTimer = 0;
				master.ResetColors();
				usedCommand();
				//changeGravity = false;
			}
		    if (stateEnable)
		    {
		        GameObject platform = GameObject.Find("UpDown");
                GameObject platform2 = GameObject.Find("UpDown2");
		        if (platform.GetComponent<Animation>() != null)
		        {
		            platform.GetComponent<Animation>().Play("On");
                    platform2.GetComponent<Animation>().Play("On2");
                    GetComponentInChildren<Animation>().Play("LeverOn");
		        }
                master.ResetColors();
		        usedCommand();    
		    }
            if (stateDisable)
            {
                GameObject platform = GameObject.Find("UpDown");
                GameObject platform2 = GameObject.Find("UpDown2");
                if (platform.GetComponent<Animation>() != null)
                {
                    platform.GetComponent<Animation>().Play("Off");
                    platform2.GetComponent<Animation>().Play("Off2");
                    GetComponentInChildren<Animation>().Play("LeverOff");
                }
                master.ResetColors();
                usedCommand();
            }
		}
		master.ResetColors();
	}

	IEnumerator fix()
	{
		yield return new WaitForSeconds(10);
		doTranslate = false;
		doRotate = false;
		doDestroy = false;
		changeGravity = false;
		stateEnable = false;
		stateDisable = false;
	}

	void usedCommand()
	{
		master.usedCommand = true;
		doTranslate = false;
		doRotate = false;
		doDestroy = false;
		changeGravity = false;
	    stateEnable = false;
	    stateDisable = false;
	}
}

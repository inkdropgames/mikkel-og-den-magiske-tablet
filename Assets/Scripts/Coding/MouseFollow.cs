﻿using UnityEngine;
using System.Collections;

public class MouseFollow : MonoBehaviour
{
	public GameObject testObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		testObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 10));
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class sliderBehaviour : MonoBehaviour
{

	public InputField input;
	public Slider slider;

	// Use this for initialization
	public void UpdateSlider()
	{
		slider.value = float.Parse(input.text);
	}

	public void UpdateInput()
	{
		input.text = slider.value.ToString("0.00");
	}
}

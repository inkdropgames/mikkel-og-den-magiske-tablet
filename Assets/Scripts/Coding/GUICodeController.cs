﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUICodeController : MonoBehaviour
{
	
	
	//Transform.Position
	public Text xTrans;
	public Text yTrans;
	public Text transTime;

	//Transform.Rotation
	public Text rotDegree;
	public Text rotTime;

	//Physics.Gravity
	public InputField gravityAmount;

	//CodeControl
	public InputField codeAreaText;

	public LevelMaster levelmaster;

	void Start()
	{
		levelmaster = GameObject.Find("LevelManager").GetComponent<LevelMaster>();	
	}
	
	public void TransformOK()
	{
		codeAreaText.text = "Transform.Translate(" + xTrans.text + ", " + yTrans.text + ") * " + transTime.text + ";";
		levelmaster.depleteScore(400);
	}

	public void RotateOK()
	{
		codeAreaText.text = "Transform.Rotate(" + rotDegree.text + ") * " + rotTime.text + ";";
		levelmaster.depleteScore(400);
	}

	public void instantiate(string resObject)
	{
		codeAreaText.text = "Instantiate(" + resObject + ");";
		levelmaster.depleteScore(400);
	}

	public void destroyObject()
	{
		codeAreaText.text = "Destroy();";
		levelmaster.depleteScore(400);
	}

	public void changeGravity()
	{
		codeAreaText.text = "Physics.Gravity(" + gravityAmount.text + ");";
		levelmaster.depleteScore(400);
	}
}

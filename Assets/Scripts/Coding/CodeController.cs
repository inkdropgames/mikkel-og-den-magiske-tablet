﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class CodeController : MonoBehaviour
{

	[Header("UI Objects")]
	public GameObject[] menu;
	public GameObject codeArea;
	public GameObject speechBuble;
	public GameObject inputField;

	[Header("UI Components")]
	public InputField codeAreaInput;
	public Text bubleText;

	[Header("UI Booleans")]
	public bool codeAreaShown;

	[Header("Misc")]
	public MovementController player;
	public Transform objectSpawner;
	public GameObject ruler;

	private string _codeInput;
    private string _codeToExecute;

	private Vector3 _mousePosition;

	private GameObject _instantiatedObject;
	public GameObject testObject;

	public GameObject placeButton;


	//Rotation and Translation
	private float _xTrans;
	private float _yTrans;
	private float _transTime;
	private float _rotDeg;
	private float _rotTime;
	private float _gravityScale = 1;

	[HideInInspector]public LevelMaster levelmaster;
	[HideInInspector]public bool doTranslation;
	[HideInInspector]public bool timeFix = true;
	[HideInInspector]public bool runCode;
	[HideInInspector]public bool executeInput = false;
	[HideInInspector]public bool usedCommand;

	[HideInInspector] public bool _placeObject;
	private bool dragging;

	//private Vector3 _oldTransPosition;

	void Start()
	{
		levelmaster = GameObject.Find("LevelManager").GetComponent<LevelMaster>();
	}

	void Update()
	{
		_codeInput = inputField.GetComponent<Text>().text;
		RunCode();

		if (doTranslation)
			testObject.transform.Translate(new Vector2(_yTrans * 1.67f / _transTime * Time.deltaTime, _xTrans * 1.67f / _transTime * Time.deltaTime));


		if (_placeObject)
		{
			if (Input.GetMouseButton(0) & !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
			{
				_mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z + 10));
				_instantiatedObject.transform.position = _mousePosition;
			}
			
			
			//if (Input.GetMouseButtonUp(0) && dragging)
			//{
			//	GameObject child = GameObject.Find("GLOW");
			//	child.SetActive(false);
			//	_instantiatedObject.GetComponentInChildren<Rigidbody2D>().gravityScale = 1;
			//	if (_instantiatedObject.GetComponentInChildren<EdgeCollider2D>() != null)
			//	{
			//		_instantiatedObject.GetComponentInChildren<EdgeCollider2D>().isTrigger = false;
			//	}
			//	if (_instantiatedObject.name.Contains("platform"))
			//	{
			//		_instantiatedObject.GetComponent<BoxCollider2D>().isTrigger = false;
			//	}
			//	_placeObject = false;
			//}
		}

		runCode = false;

		if (Input.GetKeyDown(KeyCode.Return))
		{
			if (codeAreaShown)
			{
				codeArea.SetActive(false);
				codeAreaShown = false;
			}
			else
			{
				codeArea.SetActive(true);
				codeAreaShown = true;
				if (codeAreaInput.text != "")
				{
					ExecuteCode();
				}
			}
		}

	}

	private void RunCode()
	{
		if (!runCode) return;

		speechBuble.SetActive(false);
		if (_codeInput.Contains("Instantiate") && _codeInput.Contains(";"))
		{
			_codeToExecute = _codeInput.Substring(0, _codeInput.Length - 1);
			GameObject objectClone = Instantiate(Resources.Load(_codeToExecute), objectSpawner.transform.position, Quaternion.identity) as GameObject;
			_instantiatedObject = objectClone;
			if (_instantiatedObject != null)
			{
				HideUi();
				_placeObject = true;
				usedCommand = false;
				_instantiatedObject.GetComponentInChildren<Rigidbody2D>().gravityScale = 0;
				levelmaster.depleteScore(700);
				placeButton.SetActive(true);
			}
			runCode = false;
		}
		else if (_codeInput.Contains("Ins") || _codeInput.Contains("tan") || _codeInput.Contains("iate"))
		{
			speechBuble.SetActive(true);
			bubleText.text = "Instantiate(<object>);";
			runCode = false;
		}

		if (_codeInput.Contains("Translate"))
		{
			int numberInRow = 0;
			string[] digits = Regex.Split(_codeInput, @"[-+]?([0-9]*\.[0-9]+|[0-9]+)");
			foreach (string value in digits)
			{
				float number;
				if (float.TryParse(value, out number))
				{
					switch (numberInRow)
					{
						case 0:
							_yTrans = float.Parse(value);

							if (_codeInput.Contains("(-"))
								_yTrans = _yTrans * -1f;

							numberInRow++;
							break;
						case 1:
							_xTrans = float.Parse(value);
							if (_codeInput.Contains(" -"))
								_xTrans = _xTrans * -1f;

							numberInRow++;
							break;
						case 2:
							_transTime = float.Parse(value);
							numberInRow = 0;
							break;
					}
				}
			}
			RunCommand("Translate");
			HideUi();
			runCode = false;
		}

		if (_codeInput.Contains("Transform.Rotate"))
		{
			int numberInRow = 0;
			string[] digits = Regex.Split(_codeInput, @"[-+]?([0-9]*\.[0-9]+|[0-9]+)");
			foreach (string value in digits)
			{
				float number;

				if (float.TryParse(value, out number))
				{
					if (numberInRow == 0)
					{
						_rotDeg = float.Parse(value);
						numberInRow++;

						if (!_codeInput.Contains("-"))
							_rotDeg = _rotDeg*-1f;
					}
					else if (numberInRow == 1)
					{
						_rotTime = float.Parse(value);
						numberInRow++;
					}
				}
			}
			RunCommand("Rotate");
			HideUi();
			runCode = false;
		}
		else if (_codeInput.Contains("Ro") || _codeInput.Contains("tate"))
		{
			speechBuble.SetActive(true);
			bubleText.text = "Transform.Rotate(grader)";
			inputField.GetComponent<Text>().text = "";
			runCode = false;
		}

		if (_codeInput.Contains("Physics.Gravity"))
		{
			int numberInRow = 0;
			string[] digits = Regex.Split(_codeInput, @"[-+]?([0-9]*\.[0-9]+|[0-9]+)");
			foreach (string value in digits)
			{
				float number;
				if (float.TryParse(value, out number))
				{
					if (numberInRow == 0)
					{
						_gravityScale = float.Parse(value);
						numberInRow++;
						if (_codeInput.Contains("-"))
						{
							_gravityScale = _gravityScale * -1f;
						}
					}
				}
			}
			RunCommand("Gravity");
			HideUi();
			runCode = false;
		}

		if (_codeInput == "Destroy();")
			RunCommand("Destroy");

		if (_codeInput.Contains("Enabled"))
		{
			if (_codeInput.Contains("true"))
				RunCommand("Enable");
			else if (_codeInput.Contains("false"))
				RunCommand("Disable");
		}

		if (_codeInput.Contains("Ruler.SetActive"))
		{
			if (_codeInput.Contains("true"))
				ruler.SetActive(true);

			if (_codeInput.Contains("false"))
				ruler.SetActive(false);

			HideUi();
			runCode = false;
		}
		runCode = false;
	}

	public void ExecuteCode() { runCode = true; }

	IEnumerator Translate()
	{
		doTranslation = true;
		yield return new WaitForSeconds(_transTime);
		doTranslation = false;
		ResetColors();
	}

	IEnumerator Rotate(Vector3 byAngles, float inTime)
	{
		Quaternion fromAngle = testObject.transform.rotation;
		Quaternion toAngle = Quaternion.Euler(testObject.transform.eulerAngles + byAngles);
		for (float t = 0f; t < 1; t += Time.deltaTime / inTime)
		{
			testObject.transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
			yield return null;
		}
		testObject.transform.rotation = Quaternion.Lerp(toAngle, toAngle, 0f);
	}

	public void RunCommand(string method)
	{
		usedCommand = false;
		GameObject[] nonintObjects = FindGameObjectsWithLayer(0);
		GameObject[] intObjects = FindGameObjectsWithLayer(8);

		foreach (GameObject nonintObject in nonintObjects)
		{
			if (nonintObject.GetComponent<SpriteRenderer>() != null)
				nonintObject.GetComponent<SpriteRenderer>().color = new Color(.3f, .3f, .3f);

		}
		foreach (GameObject intObject in intObjects)
		{
			if (intObject.GetComponent<HoverBehaviour>() == null)
				return;

			switch (method)
			{
				case "Translate":
					intObject.GetComponent<HoverBehaviour>().doTranslate = true;
					break;
				case "Rotate":
					intObject.GetComponent<HoverBehaviour>().doRotate = true;
					break;
				case "Destroy":
					intObject.GetComponent<HoverBehaviour>().doDestroy = true;
					break;
				case "Gravity":
					intObject.GetComponent<HoverBehaviour>().changeGravity = true;
					break;
				case "Enable":
					intObject.GetComponent<HoverBehaviour>().stateEnable = true;
					break;
				case "Disable":
					intObject.GetComponent<HoverBehaviour>().stateDisable = true;
					break;
			}
			intObject.GetComponent<HoverBehaviour>().canHover = true;
		}
		HideUi();

	}

	public void ResetColors()
	{
		GameObject[] intObjects = FindGameObjectsWithLayer(0);
		foreach (GameObject intObject in intObjects)
		{
			if (intObject.GetComponent<SpriteRenderer>() != null)
				intObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f);
		}
	}

	public void DestroyObject(GameObject objectToDestroy)
	{
		Destroy(objectToDestroy.gameObject);
	}

	public void TranslateObject(GameObject objectToTranslate)
	{
		testObject = objectToTranslate;
		StartCoroutine(Translate());
	}

	public void RotateObject(GameObject objectToRotate)
	{
		testObject = objectToRotate;
		StartCoroutine(Rotate(Vector3.forward * _rotDeg, _rotTime));
	}

	public void ChangeGravity(GameObject objectToChange)
	{
		testObject = objectToChange;
		testObject.GetComponent<Rigidbody2D>().gravityScale = _gravityScale;
	}

	GameObject[] FindGameObjectsWithLayer(int layer)
	{
		GameObject[] goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
		List<GameObject> goList = goArray.Where(t => t.layer == layer).ToList();

		if (goList.Count == 0)
			return null;

		return goList.ToArray();
	}

	public void HideUi()
	{
		foreach (GameObject menuItem in menu)
			menuItem.SetActive(false);

		codeAreaInput.text = "";
		inputField.GetComponent<Text>().text = "";
	}

	public void ShowUi()
	{
		codeAreaShown = true;
	}

	public void HideCodeUi()
	{
		codeAreaShown = false;
	}

	public void PlaceObject()
	{
		if (!_placeObject)
			return;

			try
			{
				GameObject child = GameObject.Find("GLOW");
				child.SetActive(false);
			}
			catch (Exception)
			{
				// ignored
			}
			_instantiatedObject.GetComponentInChildren<Rigidbody2D>().gravityScale = 1;
		if (_instantiatedObject.GetComponentInChildren<EdgeCollider2D>() != null)
		{
			_instantiatedObject.GetComponentInChildren<EdgeCollider2D>().isTrigger = false;
		}
		if (_instantiatedObject.name.Contains("platform"))
		{
			_instantiatedObject.GetComponent<BoxCollider2D>().isTrigger = false;
		}
		placeButton.SetActive(false);
		_placeObject = false;
	}

}

﻿using UnityEngine;
using System.Collections;

public class CodeAreaButtons : MonoBehaviour
{
    public CodeController codeController;
    public MovementController player;

    // Use this for initialization
    void OnMouseDown()
    {
        Debug.Log(name);
        if (tag == "runCode")
        {
            codeController.ExecuteCode();
        }
        else if (tag == "closeCodeArea")
        {
            //player.showCodeArea = false;
        }
    }

}